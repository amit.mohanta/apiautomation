package Actions;

import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class LoginUser extends BaseClass{
	public Response loginUserData(String email,String password) {
		RequestSpecification request = RestAssured.given().log().all();
		request.header("Content-Type","application/json");
		Map<String, String> loginCredentials = new HashMap<String, String>();
		loginCredentials.put("email", "eve.holt@reqres.in");
		loginCredentials.put("password", "cityslicka");
		JSONObject jsonData = new JSONObject();
		jsonData.putAll(loginCredentials);
		request.body(jsonData.toJSONString());
		Response response = request.post(BaseClass.prop.getProperty("baseURL")+apiResources.LoginUser.getURI());
		return response;
	}
}
