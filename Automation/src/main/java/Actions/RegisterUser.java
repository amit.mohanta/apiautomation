package Actions;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RegisterUser extends BaseClass{

	public Response registerUserData(String email, String password) {
		RequestSpecification request = RestAssured.given().log().all();
		Map<String, String> registerData = new HashMap<String, String>();
		registerData.put("email", email);
		registerData.put("password",password);
		
		JSONObject jsonData = new JSONObject();
		jsonData.putAll(registerData);
		
		request.body(jsonData.toJSONString());
		Response response = request.post(prop.getProperty("baseURL")+apiResources.RegisterUser.getURI());
		return response;
	}
}	
