package Actions;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UpdateUser {
	public Response updateUserData() {
		RequestSpecification request = RestAssured.given().log().all();
		//request.header(null, request, null)
		request.pathParams("page",2);
		Map<String, String> userDetails = new HashMap<String, String>();
		userDetails.put("name", "Amit");
		userDetails.put("Job", "QE");
		
		JSONObject json = new JSONObject();
		json.putAll(userDetails);
		request.body(json.toJSONString());
		System.out.println(BaseClass.prop.getProperty("baseURL"));
		Response response = request.put(apiResources.UpdateUser.getURI()+"/{page}");
		
		
		return response;
	}
}
