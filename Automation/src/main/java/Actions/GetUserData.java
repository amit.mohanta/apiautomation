package Actions;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetUserData extends BaseClass{
	public Response GetUserDetails() {
		RequestSpecification request = RestAssured.given().log().all();
		request.header("Context-type","applications/json");
		//request.header()
		request.queryParam("page", 2);
		//Response response = request.get("/api/users");
		//System.out.println(BaseClass.prop.getProperty("baseURL"));
		Response response = request.get(BaseClass.prop.getProperty("baseURL")+apiResources.User.getURI());
		return response;
	}
}
