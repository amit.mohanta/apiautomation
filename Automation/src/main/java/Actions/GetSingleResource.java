package Actions;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetSingleResource extends BaseClass{
	public Response getSingleResourceData(){
		int pageData = 2;
		RequestSpecification request = RestAssured.given().log().all();
		request.header("content-type","Application/json");
		request.pathParam("page",2);
		//System.out.println(BaseClass.prop.getProperty("baseURL"));
		Response response = request.get(prop.getProperty("baseURL")+apiResources.SingleResource.getURI()+"/{page}");
		return response;
	}
}
