package Actions;

import org.testng.annotations.Test;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class TestActionClass extends BaseClass{
	
	public Response getResoponseData() {
		RequestSpecification request = RestAssured.given().log().all();
		request.pathParam("page", 2);
		//System.out.println(prop.getProperty("baseURL"));
		Response response = request.delete(prop.getProperty("baseURL")+apiResources.DeleteUser.getURI()+"{/page}"); 
		return response;
	}
}
