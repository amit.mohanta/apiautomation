package Actions;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DeleteUser {
	public Response deleteUserData() {
		RequestSpecification request = RestAssured.given().log().all();
		request.header("content-type","applications/json");
		request.pathParam("page",2);
		//System.out.println(BaseClass.prop.getProperty("baseURL"));
		Response response = request.delete(BaseClass.prop.getProperty("baseURL")+apiResources.DeleteUser.getURI()+"/{page}");
		return response;
	}
}
