package Actions;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import Library.BaseClass;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class UserList extends BaseClass{
	
	public Response AddUserList(String name, String jobs){
		
		// Given 
		RequestSpecification request = RestAssured.given().log().all();
		//request.header("Content-Type","applications/json");
		Map<String,String> userListBody = new HashMap<String, String>(); 
		userListBody.put("name","Amit");
		userListBody.put("job","QA");
		//request.body(userListBody);
		
		JSONObject json = new JSONObject();
		json.putAll(userListBody);
		request.body(json.toJSONString());
		
		//When
		//Response response = request.post("api/users");
		System.out.println("Inside AddUserList"+BaseClass.prop.getProperty("baseURL"));
		//System.out.println(BaseClass.prop.getProperty("baseURL"));
		Response response = request.post(prop.getProperty("baseURL")+apiResources.CreateUser.getURI());
		return response;
		
		

	}
}
