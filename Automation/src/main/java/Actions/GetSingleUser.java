package Actions;

import Library.BaseClass;
import Library.TestBase;
import Library.apiResources;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class GetSingleUser extends BaseClass{
	public Response getSingleUserDetails() {
		RequestSpecification request = RestAssured.given().log().all();
		request.header("Content-Type","applications/json");
		request.pathParams("page",2);
		//System.out.println(BaseClass.prop.getProperty("username"));
		Response response = request.get(BaseClass.prop.getProperty("baseURL")+apiResources.SingleUser.getURI()+"/{page}");
		return response;
	}
}
