package Library;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelReader2 {
	public void readExcel(String filePath,String fileName,String sheetName) throws IOException{
	    File file = new File(filePath+"\\"+fileName);
	    FileInputStream inputStream = new FileInputStream(file);
	    Workbook workbook = null;
	    String fileExtensionName = fileName.substring(fileName.indexOf("."));
	    if(fileExtensionName.equals(".xlsx")){
	    workbook = new XSSFWorkbook(inputStream);
	    }
	    else if(fileExtensionName.equals(".xls")){
	        workbook = new HSSFWorkbook(inputStream);
	    }
	    Sheet sheet = workbook.getSheet(sheetName);
	    int rowCount = sheet.getLastRowNum();
	    System.out.println(rowCount);
	    for (int i = 1; i <= rowCount; i++) {
	        Row row = sheet.getRow(i);
	        try {
	        	for (int j = 0; j < row.getLastCellNum(); j++) {
		            System.out.print(row.getCell(j).getStringCellValue()+"|| ");
		        }

			} catch (Exception e) {
				// TODO: handle exception
			}
	        System.out.println();
	    } 

	}  

	    //Main function is calling readExcel function to read data from excel file

//	    public static void main(String...strings) throws IOException{
//
//	    //Create an object of ReadGuru99ExcelFile class
//
//	    ReadGuru99ExcelFile objExcelFile = new ReadGuru99ExcelFile();
//
//	    //Prepare the path of excel file
//
//	    String filePath = System.getProperty("user.dir")+"\\src\\excelExportAndFileIO";
//
//	    //Call read file method of the class to read data
//
//	    objExcelFile.readExcel(filePath,"ExportExcel.xlsx","ExcelGuru99Demo");
//
//	    }

}
