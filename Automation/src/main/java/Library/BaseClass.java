package Library;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class BaseClass {
	public static String baseURI = "https://reqres.in";
	public static Properties prop;
	public static File file;
	public FileInputStream fileInput;
	
	@BeforeSuite
	public void configureProperty() throws IOException {
		file = new File("C:\\Users\\amit.mohanta\\eclipse-apitesting\\Automation\\config.properties"); 
		fileInput = new FileInputStream(file);
		System.out.println(file);
		prop = new Properties();
		prop.load(fileInput);
		//System.out.println("username inside"+prop.getProperty("baseURL"));
	}
	
}
