package Library;
// for all collection of endpoints and baseurl
public enum apiResources {
	User("/api/users"),
	ListOfUser("/api/unknown"),
	SingleUser("/api/users"),
	SingleResource("/api/users"),
	CreateUser("/api/users"),
	UpdateUser("/api/users"),
	DeleteUser("/api/users"),
	RegisterUser("/api/register"),
	LoginUser("/api/login"),
	DelayResponse("/api/users");
	
	// "/api" -> base url , 
	String uri;
	//constructor
	apiResources(String uri) {
		this.uri = uri;
	}
	
	public String getURI() {
		System.out.println("_______________"+uri.toString()+"_______________");
		return uri;
		
	}
	
	
}	
