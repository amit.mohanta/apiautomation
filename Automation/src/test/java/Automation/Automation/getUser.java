package Automation.Automation;

import java.util.List;

import org.testng.annotations.Test;

import Actions.GetUserData;
import Library.BaseClass;
import io.restassured.response.Response;

public class getUser extends BaseClass{
	@Test
	public void getUser() {
		GetUserData data = new GetUserData();
		Response response2 = data.GetUserDetails();
		response2.prettyPrint();
		List<String> name = response2.getBody().jsonPath().getList("data.email");
		for (String string : name) {
			System.out.println(string);
		}
	}
}
