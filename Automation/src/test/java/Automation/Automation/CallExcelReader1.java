package Automation.Automation;
import java.io.IOException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import Library.TestBase;
import jxl.read.biff.BiffException;

public class CallExcelReader1 extends TestBase{
	@DataProvider(name = "getCredentails")
	public String[][] getPatientData() throws BiffException, IOException
	{
		String[][] creadentials = getData(System.getProperty("user.dir")+"/src/main/java/TestData/test_data.xls", 1);
		return creadentials;
	}
	
	@Test(dataProvider = "getCredentails")
	public void printExcelData(String username, String password ,String status) {
		System.out.println(username);
		System.out.println(password);
	}
	
}
