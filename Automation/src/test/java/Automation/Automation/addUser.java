package Automation.Automation;

import java.util.Properties;

import org.testng.Assert;
import org.testng.annotations.Test;

import Actions.UserList;
import Library.BaseClass;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class addUserData extends BaseClass{
	
	@Test
	public void addUser() {
		//Properties properties = new Properties();
		//RestAssured.baseURI = "https://reqres.in/"; // Make changes 		
		UserList x = new UserList();
		Response response = x.AddUserList("Amit", "Qa");
		Assert.assertEquals(response.statusCode(), 201);
		response.prettyPrint();
		String specificname  = response.body().jsonPath().getString("id");
		System.out.println(specificname);
	}
}
