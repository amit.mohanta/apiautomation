package Automation.Automation;

import org.testng.annotations.Test;

import Actions.LoginUser;
import Library.BaseClass;
import io.restassured.response.Response;

public class Login extends BaseClass{
	@Test
	public void loginUser() {
		LoginUser userDetails = new LoginUser();
		Response response = userDetails.loginUserData("eve.holt@reqres.in","cityslicka");
		response.prettyPrint();
		System.out.println(response);
				
	}
}
