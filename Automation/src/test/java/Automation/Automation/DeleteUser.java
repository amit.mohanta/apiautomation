package Automation.Automation;

import org.testng.annotations.Test;

import Actions.TestActionClass;
import Library.BaseClass;
import io.restassured.response.Response;

public class DeleteApiCall extends BaseClass{
	
	@Test
	public void deleteApiClassMethod() {
		TestActionClass delete = new TestActionClass();
		Response response = delete.getResoponseData();
		response.prettyPeek();
		System.out.println(response.statusCode());
	}
}
