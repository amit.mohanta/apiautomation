package Automation.Automation;

import org.testng.annotations.Test;

import Actions.GetSingleUser;
import Library.BaseClass;
import io.restassured.response.Response;

public class getSingleUser extends BaseClass{
	@Test
	public void getSingleUserDetails() {
		GetSingleUser singleUser = new GetSingleUser();
		Response response = singleUser.getSingleUserDetails();
		response.prettyPrint();
		System.out.println(response);
	}
}
